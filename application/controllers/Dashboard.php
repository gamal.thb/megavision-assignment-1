<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
		$this->load->view('layout/footer');
	}
	public function load_employees()
	{
		$searchColumns = array(
			'employee_id',
			'employee_name',
			'employee_email',
			'employee_phone',
			'office',
			'order_date',
			'order_item',
			'order_amount',
			'client_id',
			'client_name',
			'client_email',
			'client_phone'
		);
	
		// Get search value
		$searchValue = $this->input->get('search')['value'];
	
		// Get date range filter param
		$startDate = $this->input->get('start_date');
		$endDate = $this->input->get('end_date');
	
		$this->db->from('employees');
	
		// Search query
		if (!empty($searchValue)) {
			$this->db->group_start();
			foreach ($searchColumns as $column) {
				$this->db->or_like($column, $searchValue);
			}
			$this->db->group_end();
		}
	
		// Date range filter
		if (!empty($startDate) && !empty($endDate)) {
			$this->db->where('order_date >=', $startDate);
			$this->db->where('order_date <=', $endDate);
		}
	
		// Count the filtered records
		$filteredRecords = $this->db->count_all_results('', false);
	
		// Get column ordering parameters
		$orderColumnIndex = $this->input->get('order')[0]['column'];
		$orderDirection = $this->input->get('order')[0]['dir'];
	
		// Get column names for ordering
		$orderColumn = $this->input->get('columns')[$orderColumnIndex]['data'];
	
		// Apply column ordering
		$this->db->order_by($orderColumn, $orderDirection);
	
		// Get pagination param
		$start = $this->input->get('start');
		$length = $this->input->get('length');
	
		// Handling pagination
		if ($length !== -1) {
			$this->db->limit($length, $start);
		}
	
		// Mulai fetch data
		$employees = $this->db->get()->result();
	
		$data = array();
		foreach ($employees as $employee) {
			$data[] = array(
				'id' => $employee->id,
				'employee_id' => $employee->employee_id,
				'employee_name' => $employee->employee_name,
				'employee_email' => $employee->employee_email,
				'employee_phone' => $employee->employee_phone,
				'office' => $employee->office,
				'order_date' => date('d F Y', strtotime($employee->order_date)),
				'order_item' => $employee->order_item,
				'order_amount' => number_format($employee->order_amount, 2, ',', '.'),
				'client_id' => $employee->client_id,
				'client_name' => $employee->client_name,
				'client_email' => $employee->client_email,
				'client_phone' => $employee->client_phone
			);
		}
	
		// Prepare the JSON response
		$response = array(
			'draw' => intval($this->input->get('draw')),
			'debug' => $startDate. '-' .$endDate,
			'recordsTotal' => $this->db->count_all_results('employees'),
			'recordsFiltered' => $filteredRecords,
			'data' => $data
		);
	
		// Send the JSON response
		header('Content-Type: application/json');
		echo json_encode($response);
	}
	


}
