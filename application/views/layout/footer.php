    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url('public/vendor/jquery/jquery.min.js') ?>"></script>
    <script src="<?= base_url('public/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url('public/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url('public/js/sb-admin-2.min.js') ?>"></script>

    <!-- Page level plugins -->
    <script src="<?= base_url('public/vendor/datatables/jquery.dataTables.min.js') ?>"></script>
    <script src="<?= base_url('public/vendor/datatables/dataTables.bootstrap4.min.js') ?>"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <!-- Page level custom scripts -->
    <script src="<?= base_url('public/js/demo/datatables-demo.js') ?>"></script>
    
    <script>
    $(document).ready(function() {
        var table = $('#employees_tb').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '<?= base_url("dashboard/load_employees") ?>',
                type: 'GET'
            },
            columns: [
                { 
                    data: 'id',
                    visible: false // Hide id (biasanya diperlukan untuk kebutuhan lain)
                },
                { 
                    data: null,
                    render: function (data, type, row, meta) {
                        // Kalkulasi no.
                        var seqNumber = meta.row + meta.settings._iDisplayStart + 1;
                        return seqNumber;
                    }
                },
                { data: 'employee_id' },
                { data: 'employee_name' },
                { data: 'employee_email' },
                { data: 'employee_phone' },
                { data: 'office' },
                { data: 'order_date' },
                { data: 'order_item' },
                { 
                    data: 'order_amount',
                    className: 'text-right'
                },
                { data: 'client_id' },
                { data: 'client_name' },
                { data: 'client_email' },
                { data: 'client_phone' }
            ],
            search: {
                regex: true,
                smart: false
            },
            scrollX: true
        });

        // Apply date range filter
        $('#apply_filter').on('click', function() {
            var startDate = $('#start_date').val();
            var endDate = $('#end_date').val();

            if (!startDate || !endDate) {
                alert("Please fill in both start date and end date.");
                return;
            }

            var formattedStartDate = formatDate(startDate);
            var formattedEndDate = formatDate(endDate);

            if (new Date(formattedEndDate) < new Date(formattedStartDate)) {
                alert("End date cannot be older than the start date.");
                return;
            }

            table.ajax.url('<?= base_url("index.php/dashboard/load_employees") ?>?start_date=' + formattedStartDate + '&end_date=' + formattedEndDate).draw();
        });

        // Clear date range filter
        $('#clear_filter').on('click', function() {
            $('#start_date').val('');
            $('#end_date').val('');
            table.ajax.url('<?= base_url("index.php/dashboard/load_employees") ?>').draw();
        });
    });


    function formatDate(date) {
        if (!date) {
            return '';
        }

        var year = date.substr(6, 4);
        var month = date.substr(3, 2);
        var day = date.substr(0, 2);

        return year + '-' + month + '-' + day;
    }

    $(function () {
        $('.datepicker').datepicker({
            language: "es",
            autoclose: true,
            format: "dd/mm/yyyy"
        });
    });
    </script>

</body>

</html>